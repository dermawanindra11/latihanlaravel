@extends("layout.master")
@section("title")
Halaman Form
@endsection
@section("content")
<h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>

    <form action="/signup" method="post">
        @csrf
      <label>First name:</label><br />
      <input type="text" name="fname" /> <br />
      <br />
      <label>Last name:</label><br />
      <input type="text" name="lname" /><br /><br />
      <label>Gender</label> <br />
      <br />
      <input type="radio" name="M" /> Male <br />
      <input type="radio" name="M" /> Female <br />
      <br />
      <label>Nationality</label> <br /><br />
      <select>
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
      </select>
      <br /><br />
      <label>Language Spoken</label> <br />
      <br />
      <input type="checkbox" name="Language Spoken" /> Bahasa Indonesia <br />
      <input type="checkbox" name="Language Spoken" /> English<br />
      <input type="checkbox" name="Language Spoken" /> Other <br />
      <br />
      <label>Bio</label> <br />
      <br />
      <textarea name="bio" id="" cols="30" rows="10"></textarea> <br />
      <input type="submit" value="Sign Up" />
    </form>
    @endsection